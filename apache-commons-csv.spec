Name:                apache-commons-csv
Version:             1.12.0
Release:             1
Summary:             Utilities to assist with handling of CSV files
License:             Apache-2.0
URL:                 https://commons.apache.org/proper/commons-csv/
BuildArch:           noarch
Source0:             http://archive.apache.org/dist/commons/csv/source/commons-csv-%{version}-src.tar.gz
Source1:             xmvn-reactor

Patch1:              0001-add-javadoc-plugin.patch
BuildRequires:       maven-local 
BuildRequires:       java-1.8.0-openjdk-devel maven

%description
Commons CSV was started to unify a common and simple interface for
reading and writing CSV files under an ASL license.

%package javadoc
Summary:             API documentation for %{name}

%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n commons-csv-%{version}-src
%patch1 -p1
sed -i 's/\r//' *.txt
find -name profile.jacoco -delete
%pom_remove_plugin :maven-assembly-plugin
%pom_remove_plugin :apache-rat-plugin
%pom_remove_plugin :maven-checkstyle-plugin
%pom_remove_dep :h2
rm src/test/java/org/apache/commons/csv/CSVPrinterTest.java
%mvn_file ":{*}" %{name} @1
%mvn_alias : commons-csv:
cp %{SOURCE1} ./.xmvn-reactor
echo `pwd` > absolute_prefix.log
sed -i 's/\//\\\//g' absolute_prefix.log
absolute_prefix=`head -n 1 absolute_prefix.log`
sed -i 's/absolute-prefix/'"$absolute_prefix"'/g' .xmvn-reactor

%build
export LC_ALL=en_US.UTF-8
mvn package -DskipTests -Dtar -Drat.skip=true

%install
%mvn_install
install -d -m 0755 %{buildroot}/%{_javadocdir}/%{name}
install -m 0755 target/commons-csv-%{version}-javadoc.jar %{buildroot}/%{_javadocdir}/%{name}

%files -f .mfiles
%doc RELEASE-NOTES.txt
%license LICENSE.txt NOTICE.txt

%files javadoc
%{_javadocdir}/%{name}
%license LICENSE.txt NOTICE.txt

%changelog
* Wed Nov 06 2024 shaojiansong <shaojiansong@kylonos.cn> - 1.12.0-1
- Update to version 1.12.0

* Fri Jul 12 2024 xu_ping <707078654@qq.com> - 1.11.0-1
- Update to version 1.11.0
- Add and use CSVFormat#setTrailingData(boolean) in CSVFormat.EXCEL for Excel compatibility #303
- Replace deprecated method in user guide.

* Wed May 10 2023 chenchen <chen_aka_jan@163.com> - 1.10.0-1
- Update to version 1.10.0

* Mon Jun 13 2022 Ge Wang <wangge20@h-partners.com> - 1.9.0-1
- Upgrade to version 1.9.0

* Tue Jan 18 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.7-1
- Upgrade to version 1.7

* Mon Sep 14 2020 baizhonggui <baizhonggui@huawei.com> - 1.5-2
- Modify source0

* Sat Jul 25 2020 chengzihan <chengzihan2@huawei.com> - 1.5-1
- Package init
